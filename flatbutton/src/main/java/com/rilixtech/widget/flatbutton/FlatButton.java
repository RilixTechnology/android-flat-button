package com.rilixtech.widget.flatbutton;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

/**
 * Created by hoang8f on 5/5/14.
 * Updated by Joielechong 3/27/19.
 */
public class FlatButton extends Button implements View.OnTouchListener {
  //Custom values
  private boolean mIsShadowEnabled = true;
  private int mButtonColor;
  private int mShadowColor;
  private int mShadowHeight;
  private int mCornerRadius;
  //Native values
  private int mPaddingLeft;
  private int mPaddingRight;
  private int mPaddingTop;
  private int mPaddingBottom;
  //Background drawable
  private Drawable mPressedDrawable;
  private Drawable mUnpressedDrawable;

  boolean mIsShadowColorDefined = false;

  public FlatButton(Context context) {
    super(context);
    init();
    setOnTouchListener(this);
  }

  public FlatButton(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
    parseAttrs(context, attrs);
    setOnTouchListener(this);
  }

  public FlatButton(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init();
    parseAttrs(context, attrs);
    setOnTouchListener(this);
  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    refresh(); //Update background color
  }

  @Override
  public boolean onTouch(View view, MotionEvent motionEvent) {
    switch (motionEvent.getAction()) {
      case MotionEvent.ACTION_DOWN:
        updateBackground(mPressedDrawable);
        setPadding(mPaddingLeft, mPaddingTop + 2* mShadowHeight, mPaddingRight, mPaddingBottom);
        break;
      case MotionEvent.ACTION_MOVE:
        Rect r = new Rect();
        view.getLocalVisibleRect(r);
        if (!r.contains((int) motionEvent.getX(), (int) motionEvent.getY() + 3 * mShadowHeight) &&
            !r.contains((int) motionEvent.getX(), (int) motionEvent.getY() - 3 * mShadowHeight)) {
          updateBackground(mUnpressedDrawable);
          setPadding(mPaddingLeft, mPaddingTop + 2 * mShadowHeight, mPaddingRight,
              mPaddingBottom + mShadowHeight);
        }
        break;
      case MotionEvent.ACTION_OUTSIDE:
      case MotionEvent.ACTION_CANCEL:
      case MotionEvent.ACTION_UP:
        updateBackground(mUnpressedDrawable);
        setPadding(mPaddingLeft, mPaddingTop + mShadowHeight,
            mPaddingRight, mPaddingBottom + mShadowHeight);
        break;
    }
    return false;
  }

  private void init() {
    mIsShadowEnabled = true;
    Resources res = getResources();
    if (res == null) return;
    mButtonColor = res.getColor(R.color.fbutton_default_color);
    mShadowColor = res.getColor(R.color.fbutton_default_shadow_color);
    mShadowHeight = res.getDimensionPixelSize(R.dimen.fbutton_default_shadow_height);
    mCornerRadius = res.getDimensionPixelSize(R.dimen.fbutton_default_conner_radius);

    // this remove the shadow from button in android recent version.
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      setStateListAnimator(null);
    }
  }

  @SuppressLint("ResourceAsColor")
  private void parseAttrs(Context context, AttributeSet attrs) {
    TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.FlatButton);

    for (int i = 0; i < arr.getIndexCount(); i++) {
      int attr = arr.getIndex(i);
      if (attr == R.styleable.FlatButton_flb_shadowEnabled) {
        mIsShadowEnabled = arr.getBoolean(attr, true);
      } else if (attr == R.styleable.FlatButton_flb_buttonColor) {
        mButtonColor = arr.getColor(attr, R.color.fbutton_default_color);
      } else if (attr == R.styleable.FlatButton_flb_shadowColor) {
        mShadowColor = arr.getColor(attr, R.color.fbutton_default_shadow_color);
        mIsShadowColorDefined = true;
      } else if (attr == R.styleable.FlatButton_flb_shadowHeight) {
        mShadowHeight = arr.getDimensionPixelSize(attr, R.dimen.fbutton_default_shadow_height);
      } else if (attr == R.styleable.FlatButton_flb_cornerRadius) {
        mCornerRadius = arr.getDimensionPixelSize(attr, R.dimen.fbutton_default_conner_radius);
      }
    }
    arr.recycle();

    //Get paddingLeft, paddingRight
    int[] attrsArray = new int[] {
        android.R.attr.paddingLeft,  // 0
        android.R.attr.paddingRight, // 1
    };
    TypedArray ta = context.obtainStyledAttributes(attrs, attrsArray);

    mPaddingLeft = ta.getDimensionPixelSize(0, 0);
    mPaddingRight = ta.getDimensionPixelSize(1, 0);
    ta.recycle();

    //Get paddingTop, paddingBottom
    int[] attrsArray2 = new int[] {
        android.R.attr.paddingTop,   // 0
        android.R.attr.paddingBottom,// 1
    };
    TypedArray ta1 = context.obtainStyledAttributes(attrs, attrsArray2);

    mPaddingTop = ta1.getDimensionPixelSize(0, 0);
    mPaddingBottom = ta1.getDimensionPixelSize(1, 0);
    ta1.recycle();
  }

  public void refresh() {
    int alpha = Color.alpha(mButtonColor);
    float[] hsv = new float[3];
    Color.colorToHSV(mButtonColor, hsv);
    hsv[2] *= 0.8f; // value component
    //if shadow color was not defined, generate shadow color = 80% brightness
    if (!mIsShadowColorDefined) mShadowColor = Color.HSVToColor(alpha, hsv);

    //Create pressed background and unpressed background drawables
    if (isEnabled()) {
      if (mIsShadowEnabled) {
        mPressedDrawable = createDrawable(mCornerRadius, Color.TRANSPARENT, mButtonColor);
        mUnpressedDrawable = createDrawable(mCornerRadius, mButtonColor, mShadowColor);
      } else {
        mShadowHeight = 0;
        mPressedDrawable = createDrawable(mCornerRadius, mShadowColor, Color.TRANSPARENT);
        mUnpressedDrawable = createDrawable(mCornerRadius, mButtonColor, Color.TRANSPARENT);
      }
    } else {
      Color.colorToHSV(mButtonColor, hsv);
      hsv[1] *= 0.25f; // saturation component
      int disabledColor = mShadowColor = Color.HSVToColor(alpha, hsv);
      // Disabled button does not have shadow
      mPressedDrawable = createDrawable(mCornerRadius, disabledColor, Color.TRANSPARENT);
      mUnpressedDrawable = createDrawable(mCornerRadius, disabledColor, Color.TRANSPARENT);
    }
    updateBackground(mUnpressedDrawable);
    setPadding(mPaddingLeft, mPaddingTop + mShadowHeight, mPaddingRight,
        mPaddingBottom + mShadowHeight);
  }

  /**
   * Set button background
   *
   * @param background background for button
   */
  private void updateBackground(Drawable background) {
    if (background == null) return;
    if (Build.VERSION.SDK_INT >= 16) {
      setBackground(background);
    } else {
      setBackgroundDrawable(background);
    }
  }

  private LayerDrawable createDrawable(int radius, int topColor, int bottomColor) {
    float[] outerRadius = new float[] { radius, radius, radius, radius, radius, radius, radius, radius };

    //Top
    RoundRectShape topRoundRect = new RoundRectShape(outerRadius, null, null);
    ShapeDrawable topShapeDrawable = new ShapeDrawable(topRoundRect);
    topShapeDrawable.getPaint().setColor(topColor);
    //Bottom
    RoundRectShape roundRectShape = new RoundRectShape(outerRadius, null, null);
    ShapeDrawable bottomShapeDrawable = new ShapeDrawable(roundRectShape);
    bottomShapeDrawable.getPaint().setColor(bottomColor);
    //Create array
    Drawable[] drawArray = {bottomShapeDrawable, topShapeDrawable};
    LayerDrawable layerDrawable = new LayerDrawable(drawArray);

    //Set shadow height
    if (mIsShadowEnabled && topColor != Color.TRANSPARENT) {
      //unpressed drawable
      layerDrawable.setLayerInset(0, 0, 0, 0, 0);
    } else {
      //pressed drawable
      layerDrawable.setLayerInset(0, 0, mShadowHeight, 0, 0);
    }
    layerDrawable.setLayerInset(1, 0, 0, 0, mShadowHeight);
    return layerDrawable;
  }

  public void setShadowEnabled(boolean isShadowEnabled) {
    this.mIsShadowEnabled = isShadowEnabled;
    setShadowHeight(0);
    refresh();
  }

  public void setButtonColor(int buttonColor) {
    mButtonColor = buttonColor;
    refresh();
  }

  public void setShadowColor(int shadowColor) {
    mShadowColor = shadowColor;
    mIsShadowColorDefined = true;
    refresh();
  }

  public void setShadowHeight(int shadowHeight) {
    mShadowHeight = shadowHeight;
    refresh();
  }

  public void setCornerRadius(int cornerRadius) {
    mCornerRadius = cornerRadius;
    refresh();
  }

  public void setButtonPadding(int left, int top, int right, int bottom) {
    mPaddingLeft = left;
    mPaddingRight = right;
    mPaddingTop = top;
    mPaddingBottom = bottom;
    refresh();
  }

  @Override
  public void setEnabled(boolean enabled) {
    super.setEnabled(enabled);
    refresh();
  }

  public boolean isShadowEnabled() {
    return mIsShadowEnabled;
  }

  public int getButtonColor() {
    return mButtonColor;
  }

  public int getShadowColor() {
    return mShadowColor;
  }

  public int getShadowHeight() {
    return mShadowHeight;
  }

  public int getCornerRadius() {
    return mCornerRadius;
  }
}
